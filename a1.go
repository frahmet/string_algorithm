package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var k int
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("text?: ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSuffix(text, "\n")
	fmt.Println("k?: ")
	fmt.Scanf("%d", &k)
	subStrings := findSubStrings(text)
	fmt.Println("Count : ", calculate(text, subStrings, k))
	// fmt.Print(len(findSubStrings(text)))
}

func findSubStrings(text string) map[string]int {
	subStrings := make(map[string]int)
	for i := 0; i < len(text); i++ {
		for j := i + 1; j <= len(text)-1; j++ {
			if val, ok := subStrings[text[i:j]]; ok {
				subStrings[text[i:j]] = val + 1
			} else {
				subStrings[text[i:j]] = 1
			}
		}
	}
	return subStrings
}

func isThereIsland(arr []int, k int) bool {
	count := 0
	index := 0
	for index < len(arr) {
		if arr[index] > 0 {
			count += 1
			for index < len(arr) && arr[index] > 0 {
				index += 1
			}
		}
		index += 1
	}
	if count == k {
		return true
	} else {
		return false
	}
}

func calculate(text string, subStrings map[string]int, k int) int {
	count := 0
	for subString, _ := range subStrings {
		// fmt.Print(subString)
		island := make([]int, int(len(text)))
		for i := 0; i < len(text)-len(subString)+1; i++ {
			if text[i:i+len(subString)] == subString {
				j := i
				for range subString {
					island[j] += 1
					j += 1
				}
			}
		}
		// fmt.Print(island)
		if isThereIsland(island, k) {
			count += 1
			fmt.Println("Subring : ", subString)
		}
	}
	return count
}
